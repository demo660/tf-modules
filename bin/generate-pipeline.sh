#!/bin/bash

modules_base_dir="modules"

is_new(){
  equal=0
  tf_module=$1
  tf_version=$(cat "${tf_module}/module.json" | jq -r '.version')
  curl -XGET -H "Authorization: Bearer ${CI_JOB_TOKEN}" \
  "${CI_API_V4_URL}/packages/terraform/modules/v1/${CI_PROJECT_NAMESPACE}/${tf_module}/versions" \
  | jq -r '.modules | .[] | .versions | .[] | .version' > t.txt
  
  while read ver;
    do
      if [ "$ver" = "$tf_version" ]; then
       equal=1
      fi
    done < t.txt
  if [ $equal = 1 ]; then
    return 1
  else
    return 2
  fi
}



# needed for so the "cat" commant won't evaluate this variables
# version='${version}'
# file_name='${file_name}'
# CI_JOB_TOKEN='${CI_JOB_TOKEN}'
# CI_API_V4_URL='${CI_API_V4_URL}'
# CI_PROJECT_ID='${CI_PROJECT_ID}'
# CI_PROJECT_NAMESPACE='${CI_PROJECT_NAMESPACE}'

export i=0
ls -d $modules_base_dir/* | xargs -L1 basename | while read -r module
do
  if ! (test -f "${modules_base_dir}/${module}/module.json"); then
    echo "no module.json file found for ${module}!" >&2
    continue
  fi

  is_new $module $modules_base_dir
  res=$?

  if [ $res = 2 ]; then
  ((i=i+1))
	cat << EOF

${module}_deploy:
    image: "registry.gitlab.com/tikal-external/academy-public/images/ci-job"
    stage: deploy
    script:
      - bin/deploy.sh ${modules_base_dir} ${module}
    only:
      - main
EOF
fi
done

if (( $i == 0 )); then
echo "no jobs needed. creating default no_chages" >&2
	cat << EOF
no_changes:
    stage: deploy
    script:
      - echo "no changes in modules"
    
EOF
fi
