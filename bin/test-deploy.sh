# aws_modules=$(ls -d aws/* | xargs -L1 basename)

modules_base_dir=modules
mkdir -p tmp
ls -d ${modules_base_dir}/* | xargs -L1 basename | while read -r module
do

    #tests.....

    ## DEPLOY
    echo "deploy ${module} ..."
    version=$(cat "${modules_base_dir}/${module}/module.json" | jq -r '.version')
    echo "${module} --> ${version}"
    file_name="tmp/${module}-${version}.tgz"
    
    #make tgz to push to registry
    tar -cvzf $file_name -C "${basedir}/${module}" --exclude=./.terraform --exclude=./.git --exclude=./.terraform.lock .
    
    # pushing to registry. Token should be supplied by ci job or manualy as deploy token
    # read: https://docs.gitlab.com/ee/user/packages/terraform_module_registry/
    curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file $file_name ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${module}/${version}/file
done

