#!/bin/sh


modules_base_dir=$1
module=$2

version=$(cat "${modules_base_dir}/${module}/module.json" | jq -r '.version')
file_name="${module}-${version}.tgz"


tar -cvzf $file_name -C "${modules_base_dir}/${module}" --exclude=./.terraform --exclude=./.git --exclude=./.terraform.lock .
response=$(curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    --upload-file $file_name ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${module}/${version}/file)

msg=$(echo $response | jq '.message')
if [ $msg = "null" ]; then
 echo "Fail to push to terraform registry. Release already Exists!"
 exit 1;
fi

