data "gitlab_user" "owner" {
  username = var.gitlab_project_owner
}

data "gitlab_group" "parent" {
  full_path = var.gitlab_parent_group_name
}
