variable "gitlab_token" {
  default     = null
  description = "The GitLab Personal Access Token"
}

variable "gitlab_project_owner" {
  default     = null
  description = "The GitLab API token"
}

variable "gitlab_parent_group_name" {
  default     = null
  description = "The GitLab API token"
}
