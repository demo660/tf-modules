resource "gitlab_group" "external" {
  name                = "external"
  description         = "Public / External Projects"
  path                = "external"
  parent_id           = data.gitlab_group.parent.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "external_demo" {
  name                = "demo"
  description         = "Demo Projects"
  path                = "demo"
  parent_id           = gitlab_group.external.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}


resource "gitlab_group" "external_apps" {
  name                = "apps"
  description         = "Microservices"
  path                = "apps"
  parent_id           = gitlab_group.external.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "external-infra" {
  name                = "infra"
  description         = "Infra As Code projects"
  path                = "infra"
  parent_id           = gitlab_group.external.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "external_utils" {
  name                = "utils"
  description         = "Infra As Code projects"
  path                = "utils"
  parent_id           = gitlab_group.external.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}
