
output "internal_project_id" { value = gitlab_group.internal.id }
output "internal_demo_project_id" { value = gitlab_group.internal_demo.id }
output "internal_apps_project_id" { value = gitlab_group.internal_apps.id }
output "internal_infra_project_id" { value = gitlab_group.internal_infra.id }
output "internal_utils_project_id" { value = gitlab_group.internal_utils.id }

output "external_project_id" { value = gitlab_group.external.id }
output "external_demo_project_id" { value = gitlab_group.external_demo.id }
output "external_apps_project_id" { value = gitlab_group.external_apps.id }
output "external_infra_project_id" { value = gitlab_group.external-infra.id }
output "external_utils_project_id" { value = gitlab_group.external_utils.id }
