resource "gitlab_group" "internal" {
  name                = "internal"
  description         = "Tools & Utilities Projects"
  path                = "internal"
  parent_id           = data.gitlab_group.parent.id
  visibility_level    = "private"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "internal_demo" {
  name                = "demo"
  description         = "Demo Projects"
  path                = "demo"
  parent_id           = gitlab_group.internal.id
  visibility_level    = "private"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "internal_apps" {
  name                = "apps"
  description         = "Microservices"
  path                = "apps"
  parent_id           = gitlab_group.internal.id
  visibility_level    = "private"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "internal_infra" {
  name                = "infra"
  description         = "Infra As Code projects"
  path                = "infra"
  parent_id           = gitlab_group.internal.id
  visibility_level    = "private"
  auto_devops_enabled = "false"
}

resource "gitlab_group" "internal_utils" {
  name                = "utils"
  description         = "Infra As Code projects"
  path                = "utils"
  parent_id           = gitlab_group.internal.id
  visibility_level    = "private"
  auto_devops_enabled = "false"
}
